express = require 'express'
Twit = require 'twit'
router = express.Router()
config = require('../config')

# instantiate Twit module
twitter = new Twit(config.twitter)

TWEET_COUNT = 15
USER_TIMELINE_URL = 'statuses/user_timeline'
SEARCH_TWEETS_URL = 'search/tweets'


# GET home page.
router.get '/', (req, res) ->
  res.render 'index', { title: 'Get a Twitter feed...' }


# GET user tweets.
router.get '/tweets/user_timeline/:user', (req, res) ->

  tweets = []
  params =
    screen_name: req.params.user
    count: TWEET_COUNT

  # request data from twitter
  twitter.get USER_TIMELINE_URL, params, (err, data, resp) ->
    tweets = data
    res.setHeader('Content-Type', 'application/json')
    res.send(tweets)

# GET tweets by hashtag
router.get '/tweets/search_tweets/:search_term', (req, res) ->

  tweets = []
  params =
    q: '#' + req.params.search_term
    count: TWEET_COUNT

  # request data from twitter
  twitter.get SEARCH_TWEETS_URL, params, (err, data, resp) ->
    console.log("data===>>>>")
    console.log(data)
    tweets = data
    res.setHeader('Content-Type', 'application/json')
    res.send(tweets)


module.exports = router
