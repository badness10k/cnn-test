var app = angular.module('CNN-Twitter-app', ['ngResource']);

app.controller('TweetList', function($scope, $resource, $timeout) {

  $scope.getTweets = function() {

    $scope.tweetsResult = [];

    var params = {};

    // create Tweet data resource based on search term
    if ($scope.searchTerm.charAt(0) == '@') {
      params.action = 'user_timeline';
      params.user = $scope.searchTerm.substring(1);
    }
    else if ($scope.searchTerm.charAt(0) == '#') {
      // TODO: fix this
      params.action = 'search_tweets';
      params.search_term = $scope.searchTerm.substring(1);

      // /tweets/search_tweets/:searchTerm
      $scope.tweets = $resource('/tweets/:action/:search_term', params);
    }

    // create Tweet data resource
    $scope.tweets = $resource('/tweets/:action/:user', params);

    // GET request using the resource
    $scope.tweets.query({}, function(res) {
      // console.log(JSON.stringify(res));

      $scope.tweetsResult = $scope.tweetsResult.concat(res);
    }, function(error) { console.log(JSON.stringify(error)); });

  }
});
