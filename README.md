CNN Developer Interview Challenge
=================================

Instructions
------------

* You will have 24 hours to complete this practical test.
* Please make your completed work is available via a GitHub repo and send me the link for review.
* Your README must provide instructions on how to install and run your application.

Description
-----------
We would like you to develop a responsive page that displays twitter feeds. We would like you to show us your cool front-end skills and also your ability to develop a node.js server (HAPI or Express, etc) that uses the Twitter API to get a feed.  This is an opportunity to show us how you develop. We want to see your coding style, use of open source libs, testing approach, documentation, creativity, etc.  You may use any open source libs or available code to support your solution but you must be able to explain your code if you are selected to come in for an in-person interview.  If you have questions, you may send them to me ian.moraes@turner.com but no immediate responses should be expected.  State any assumptions and move on as the clock for completing the task does not pause.

Install Instructions
--------------------
1. Create an app on twitter by going to [apps.twitter.com](https://apps.twitter.com)
    * you'll need a twitter account, with your phone number linked to it
2. Rename `config.sample.js` to `config.js` and replace the placeholders with your keys and tokens from your twitter app.
3. Use `npm install` to grab dependencies.
4. Running:
    * Use `npm start` to run, or
    * Use `gulp` for development, this starts watching files for changes, livereload, etc.

TODO
----

* implement hashtag searching
* implement trending tweets
* add animation for tweets appearing/disappearing
* testing coverage
